import { Header } from "./components/Header";
import { useState } from "react";
import { TodoList } from "./components/TodoList";


function App() {
  const [todosList, setTodosList] = useState([])


  return (
      <div className="App">
        <Header setTodosList={setTodosList}/>
        <TodoList todosList={todosList} />
      </div>
  );
}

export default App;
