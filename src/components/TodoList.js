import React from 'react'

export const TodoList = (props) => {
    console.log(props)
  return (
    <div>
        <h2>Todo List:</h2>
        <ul>
            {props.todosList.map((todo, index) => <li key={index}>{todo.name}</li>)}
        </ul>
    </div>
  )
}