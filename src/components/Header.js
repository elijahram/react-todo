import { useState } from 'react'

export const Header = (props) => {
    const [todoAdd, setTodoAdd] = useState("")

    const handleOnChange = (e) => {
        setTodoAdd(e.target.value)
    }

    const handleAddTodo = () => {
        props.setTodosList((prev) => {
            const todosClone=[...prev]
            const newTodo = {
                name: todoAdd,
                completed: false,
            }
            todosClone.push(newTodo)
            setTodoAdd("")
            return todosClone
        })
    }

    return (
    <>
        <h2>
            Create Todo
        </h2>
        <div>
            Name:
            <input type="text" value={todoAdd} onChange={handleOnChange} />
            <button onClick={handleAddTodo}>Add</button>
        </div>
    </>
    )
}
